package com.example.dagger2

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
@ContributesAndroidInjector
internal abstract fun bindMainActivity(): MainActivity

}