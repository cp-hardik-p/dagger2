package com.example.dagger2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main2.*
import javax.inject.Inject

class MainActivity2 : AppCompatActivity() {
   // var component: AppComponent = DaggerAppComponent.builder().build()

    @Inject
    lateinit var info: Dependency

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
    //    component.inject2(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        tvDemo.text = info.text1
        tvDemo2.text = info.repeat3Times(R.string.app_name)


    }
}

