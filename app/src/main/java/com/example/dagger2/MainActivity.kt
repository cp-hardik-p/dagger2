package com.example.dagger2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

//    var component: AppComponent = DaggerAppComponent.builder().build()

    @Inject
    lateinit var info: Dependency

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        //  component.inject(this)
        //    (application as MainApplication).component.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvDemo.text = info.repeat3Times(R.string.app_name)
          tvDemo2.text = info.text1

        btn1.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }
    }
}