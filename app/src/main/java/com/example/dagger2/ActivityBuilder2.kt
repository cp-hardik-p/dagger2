package com.example.dagger2

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder2 {
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity2
}