package com.example.dagger2

import android.app.Application
import androidx.annotation.StringRes
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Dependency @Inject constructor(private val application: Application) {

      fun repeat3Times(@StringRes stringRes: Int): String =
            "${application.getString(stringRes)} ${application.getString(stringRes)} ${application.getString(stringRes)}"

      var text = "hayabusa"
      var text1 = "canopas"
}